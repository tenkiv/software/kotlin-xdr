object Vof {
    // common main
    const val kotlin = "1.3.60"
    const val serialization = "0.14.0"
    const val spek = "2.0.8"

    // JVM test
    const val javaStellarSdk = "0.10.0"
}