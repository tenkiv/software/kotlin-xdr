object Info {
    const val pomDescription = "A Kotlinx.Serialization addon format for XDR."
    const val pomLicense = "MIT License"
    const val pomLicenseUrl = "https://opensource.org/licenses/MIT"
    const val pomOrg = "Tenkiv, Inc."
    const val sonatypeReleaseRepoUrl = "https://oss.sonatype.org/service/local/staging/deploy/maven2"
    const val sonatypeSnapshotRepoUrl = "https://oss.sonatype.org/content/repositories/snapshots"
    const val projectUrl = "https://gitlab.com/tenkiv/software/kotlin-xdr.git"
    const val projectCloneUrl = "git@gitlab.com:tenkiv/software/kotlin-xdr.git"
    const val projectDevEmail = "zjuhasz@tenkiv.org"
    const val projectDevEmail2 = "skern@tenkiv.org"
}