package tenkiv.xdr

import kotlinx.serialization.*
import org.spekframework.spek2.*
import org.spekframework.spek2.style.specification.*
import kotlin.test.*

object CollectionSerializationSpec : Spek({
    describe("A byte list") {
        val byteList = listOf(0.toByte(), 1.toByte(), 2.toByte(), 3.toByte(), 4.toByte())

        describe("serialization and deserialization") {

            it("should successfully be serialized to binary") {
                Xdr.dump(Byte.serializer().list, byteList)
            }

            it("should be successfully deserialized from binary") {
                val serialized = Xdr.dump(Byte.serializer().list, byteList)
                val deserialized = Xdr.load(Byte.serializer().list, serialized)
                assertEquals(expected = byteList, actual = deserialized)
            }
        }
    }

    describe("An value backed by fixed length byte array") {
        val arrayWrapper = ByteArrayWrapper(byteArrayOf(1, 2, 3, 4, 5))

        describe("serialization and deserialization") {

            it("should successfully be serialized to binary") {
                Xdr.dump(ByteArrayWrapper.serializer(), arrayWrapper)
            }

            it("should be successfully deserialized from binary") {
                val serialized = Xdr.dump(ByteArrayWrapper.serializer(), arrayWrapper)
                val deserialized = Xdr.load(ByteArrayWrapper.serializer(), serialized)
                assertEquals(expected = arrayWrapper, actual = deserialized)
            }
        }
    }
})

@Serializable
data class ByteArrayWrapper(@FixedLengthXdrArray(5) val bytes: ByteArray) {
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ByteArrayWrapper

        if (!bytes.contentEquals(other.bytes)) return false

        return true
    }

    override fun hashCode(): Int {
        return bytes.contentHashCode()
    }
}