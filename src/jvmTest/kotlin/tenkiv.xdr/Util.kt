package tenkiv.xdr

import org.stellar.sdk.xdr.*
import java.io.*

fun XdrInputStream(bytes: ByteArray): XdrDataInputStream = XdrDataInputStream(ByteArrayInputStream(bytes))