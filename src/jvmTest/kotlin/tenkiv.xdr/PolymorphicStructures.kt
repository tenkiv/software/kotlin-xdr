package tenkiv.xdr

import kotlinx.serialization.*
import org.spekframework.spek2.*
import org.spekframework.spek2.style.specification.*
import kotlin.test.*

object PolymorphicStructuresSpec : Spek({
    describe("Some polymorphic types") {
        val defaultObject = DefaultPolymorphic.TypeOne
        val defaultData = DefaultPolymorphic.TypeThree("information")

        val unionObject = XdrUnionPolymorphic.TypeOne
        val unionData = XdrUnionPolymorphic.TypeThree("information")

        val singleSubtypeDefault = SingleSubtypeDefault.Single
        val singleSubtypeUnion = SingleSubtypeUnion.Single

        describe("default object serialization and deserialization") {

            it("should successfully be serialized to binary") {
                Xdr.dump(DefaultPolymorphic.serializer(), defaultObject)
            }

            it("should be successfully deserialized from binary") {
                val serialized = Xdr.dump(DefaultPolymorphic.serializer(), defaultObject)
                val deserialized = Xdr.load(DefaultPolymorphic.serializer(), serialized)
                assertEquals(expected = defaultObject, actual = deserialized)
            }

        }

        describe("default data serialization and deserialization") {

            it("should successfully be serialized to binary") {
                Xdr.dump(DefaultPolymorphic.serializer(), defaultData)
            }

            it("should be successfully deserialized from binary") {
                val serialized = Xdr.dump(DefaultPolymorphic.serializer(), defaultData)
                val deserialized = Xdr.load(DefaultPolymorphic.serializer(), serialized)
                assertEquals(expected = defaultData, actual = deserialized)
            }

        }

        describe("xdr union object serialization and deserialization") {

            it("should successfully be serialized to binary") {
                Xdr.dump(XdrUnionPolymorphic.serializer(), unionObject)
            }

            it("should be successfully deserialized from binary") {
                val serialized = Xdr.dump(XdrUnionPolymorphic.serializer(), unionObject)
                val deserialized = Xdr.load(XdrUnionPolymorphic.serializer(), serialized)
                assertEquals(expected = unionObject, actual = deserialized)
            }

        }

        describe("xdr union data serialization and deserialization") {

            it("should successfully be serialized to binary") {
                Xdr.dump(XdrUnionPolymorphic.serializer(), unionData)
            }

            it("should be successfully deserialized from binary") {
                val serialized = Xdr.dump(XdrUnionPolymorphic.serializer(), unionData)
                val deserialized = Xdr.load(XdrUnionPolymorphic.serializer(), serialized)
                assertEquals(expected = unionData, actual = deserialized)
            }

        }

        describe("default single type data serialization and deserialization") {

            it("should successfully be serialized to binary") {
                Xdr.dump(SingleSubtypeDefault.serializer(), singleSubtypeDefault)
            }

            it("should be successfully deserialized from binary") {
                val serialized = Xdr.dump(SingleSubtypeDefault.serializer(), singleSubtypeDefault)
                val deserialized = Xdr.load(SingleSubtypeDefault.serializer(), serialized)
                assertEquals(expected = singleSubtypeDefault, actual = deserialized)
            }

        }

        describe("xdr union single type data serialization and deserialization") {

            it("should successfully be serialized to binary") {
                Xdr.dump(SingleSubtypeUnion.serializer(), singleSubtypeUnion)
            }

            it("should be successfully deserialized from binary") {
                val serialized = Xdr.dump(SingleSubtypeUnion.serializer(), singleSubtypeUnion)
                val deserialized = Xdr.load(SingleSubtypeUnion.serializer(), serialized)
                assertEquals(expected = singleSubtypeUnion, actual = deserialized)
            }

        }

    }

})

@Serializable
sealed class DefaultPolymorphic {

    @Serializable
    object TypeOne : DefaultPolymorphic()

    @Serializable
    object TypeTwo : DefaultPolymorphic()

    @Serializable
    data class TypeThree(val extras: String) : DefaultPolymorphic()

}

@Serializable
@XdrUnion
sealed class XdrUnionPolymorphic {

    @Serializable
    @SerialName(0.toString())
    object TypeOne : XdrUnionPolymorphic()

    @Serializable
    @SerialName(1.toString())
    object TypeTwo : XdrUnionPolymorphic()

    @Serializable
    @SerialName(2.toString())
    data class TypeThree(val extras: String) : XdrUnionPolymorphic()

}

@Serializable
sealed class SingleSubtypeDefault {

    @Serializable
    object Single : SingleSubtypeDefault()

}

@Serializable
@XdrUnion
sealed class SingleSubtypeUnion {

    @Serializable
    @SerialName(0.toString())
    object Single : SingleSubtypeUnion()

}