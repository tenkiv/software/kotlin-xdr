package tenkiv.xdr

import kotlinx.serialization.*
import org.spekframework.spek2.*
import org.spekframework.spek2.style.specification.*
import kotlin.test.*

object StructureSerializationSpec : Spek({
    describe("A structure of primitives") {
        val testStructure = TestStructure(
            1,
            "abcd",
            listOf(1.0, 2.0, 3.0),
            mapOf("a" to "b", "c" to "d"),
            null
        )

        describe("serialization and deserialization") {

            it("should successfully be serialized to binary") {
                Xdr.dump(TestStructure.serializer(), testStructure)
            }

            it("should be successfully deserialized from binary") {
                val serialized = Xdr.dump(TestStructure.serializer(), testStructure)
                val deserialized = Xdr.load(TestStructure.serializer(), serialized)
                assertEquals(expected = testStructure, actual = deserialized)
            }
        }
    }
})

@Serializable
data class TestStructure(
    val aInt: Int,
    val aString: String,
    @FixedLengthXdrArray(3) val aList: List<Double>,
    val aMap: Map<String, String>,
    val nullableChar: Char?,
    val nullableBoolean: Boolean? = false
)