package tenkiv.xdr

import kotlinx.serialization.Serializable
import org.spekframework.spek2.*
import org.spekframework.spek2.style.specification.*
import kotlin.test.*

object EnumsSerializationSpec : Spek({
    describe("Some enums") {
        val default = DefaultIdentifiers.C
        val custom = CustomIdentifiers.C

        describe("default serialization and deserialization") {
            it("should successfully be serialized to binary") {
                Xdr.dump(DefaultIdentifiers.serializer(), default)
            }

            it("should be successfully deserialized from binary") {
                val serialized = Xdr.dump(DefaultIdentifiers.serializer(), default)
                val deserialized = Xdr.load(DefaultIdentifiers.serializer(), serialized)
                assertEquals(expected = default, actual = deserialized)
            }
        }

        describe("custom identifier serialization and deserialization") {
            it("should successfully be serialized to binary") {
                Xdr.dump(CustomIdentifiers.serializer(), custom)
            }

            it("should be successfully deserialized from binary") {
                val serialized = Xdr.dump(CustomIdentifiers.serializer(), custom)
                val deserialized = Xdr.load(CustomIdentifiers.serializer(), serialized)
                assertEquals(expected = custom, actual = deserialized)
            }
        }
    }
})

@Serializable
enum class DefaultIdentifiers {
    A, B, C
}

@XdrCustomEnumIdentifiers
@Serializable
enum class CustomIdentifiers {
    @XdrEnumIdentifier(-1) A,
    @XdrEnumIdentifier(-2) B,
    @XdrEnumIdentifier(-3) C
}