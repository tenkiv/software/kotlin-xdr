package tenkiv.xdr

import kotlinx.serialization.*
import org.spekframework.spek2.*
import org.spekframework.spek2.style.specification.*
import org.stellar.sdk.xdr.*
import java.io.*
import kotlin.test.*

object IntSerializationSpec : Spek({
    describe("Some Ints") {
        val tiny = Int.MIN_VALUE
        val huge = Int.MAX_VALUE
        val zero = 0
        val minusOne = -1
        val plusOne = 1

        describe("serialization") {
            it("can be reformed using stellar java xdr data types") {
                // MIN
                val tinyBytes = Xdr.dump(Int.serializer(), tiny)
                val tinyLoadResult = Int32.decode(XdrInputStream(tinyBytes))
                assertEquals(expected = tiny, actual = tinyLoadResult.int32)

                // MAX
                val hugeBytes = Xdr.dump(Int.serializer(), huge)
                val hugeLoadResult = Int32.decode(XdrInputStream(hugeBytes))
                assertEquals(expected = huge, actual = hugeLoadResult.int32)

                // 0
                val zeroBytes = Xdr.dump(Int.serializer(), zero)
                val zeroLoadResult = Int32.decode(XdrInputStream(zeroBytes))
                assertEquals(expected = zero, actual = zeroLoadResult.int32)

                // -1
                val minusOneBytes = Xdr.dump(Int.serializer(), minusOne)
                val minusOneLoadResult = Int32.decode(XdrInputStream(minusOneBytes))
                assertEquals(expected = minusOne, actual = minusOneLoadResult.int32)

                // 1
                val plusOneBytes = Xdr.dump(Int.serializer(), plusOne)
                val plusOneLoadResult = Int32.decode(XdrInputStream(plusOneBytes))
                assertEquals(expected = plusOne, actual = plusOneLoadResult.int32)
            }
        }

        describe("deserialization") {
            it("gets the correct result from stellar java xdr data type binary output") {
                val tinyXdr = Int32().apply { int32 = tiny }
                val tinyOutputStream = ByteArrayOutputStream()
                tinyXdr.encode(XdrDataOutputStream(tinyOutputStream))
                val tinyResult = Xdr.load(Int.serializer(), tinyOutputStream.toByteArray())
                assertEquals(expected = tinyXdr.int32, actual = tinyResult)

                val hugeXdr = Int32().apply { int32 = huge }
                val hugeOutputStream = ByteArrayOutputStream()
                hugeXdr.encode(XdrDataOutputStream(hugeOutputStream))
                val hugeResult = Xdr.load(Int.serializer(), hugeOutputStream.toByteArray())
                assertEquals(expected = hugeXdr.int32, actual = hugeResult)

                val zeroXdr = Int32().apply { int32 = zero }
                val zeroOutputStream = ByteArrayOutputStream()
                zeroXdr.encode(XdrDataOutputStream(zeroOutputStream))
                val zeroResult = Xdr.load(Int.serializer(), zeroOutputStream.toByteArray())
                assertEquals(expected = zeroXdr.int32, actual = zeroResult)

                val minusOneXdr = Int32().apply { int32 = minusOne }
                val minusOneOutputStream = ByteArrayOutputStream()
                minusOneXdr.encode(XdrDataOutputStream(minusOneOutputStream))
                val minusOneResult = Xdr.load(Int.serializer(), minusOneOutputStream.toByteArray())
                assertEquals(expected = minusOneXdr.int32, actual = minusOneResult)

                val plusOneXdr = Int32().apply { int32 = plusOne }
                val plusOneOutputStream = ByteArrayOutputStream()
                plusOneXdr.encode(XdrDataOutputStream(plusOneOutputStream))
                val plusOneResult = Xdr.load(Int.serializer(), plusOneOutputStream.toByteArray())
                assertEquals(expected = plusOneXdr.int32, actual = plusOneResult)

            }
        }
    }
})

object LongSerializationSpec : Spek({
    describe("Some Longs") {
        val tiny = Long.MIN_VALUE
        val huge = Long.MAX_VALUE
        val zero = 0L
        val minusOne = -1L
        val plusOne = 1L

        describe("serialization") {
            it("can be reformed using stellar java xdr data types") {
                // MIN
                val tinyBytes = Xdr.dump(Long.serializer(), tiny)
                val tinyLoadResult = Int64.decode(XdrInputStream(tinyBytes))
                assertEquals(expected = tiny, actual = tinyLoadResult.int64)

                // MAX
                val hugeBytes = Xdr.dump(Long.serializer(), huge)
                val hugeLoadResult = Int64.decode(XdrInputStream(hugeBytes))
                assertEquals(expected = huge, actual = hugeLoadResult.int64)

                // 0
                val zeroBytes = Xdr.dump(Long.serializer(), zero)
                val zeroLoadResult = Int64.decode(XdrInputStream(zeroBytes))
                assertEquals(expected = zero, actual = zeroLoadResult.int64)

                // -1
                val minusOneBytes = Xdr.dump(Long.serializer(), minusOne)
                val minusOneLoadResult = Int64.decode(XdrInputStream(minusOneBytes))
                assertEquals(expected = minusOne, actual = minusOneLoadResult.int64)

                // 1
                val plusOneBytes = Xdr.dump(Long.serializer(), plusOne)
                val plusOneLoadResult = Int64.decode(XdrInputStream(plusOneBytes))
                assertEquals(expected = plusOne, actual = plusOneLoadResult.int64)
            }
        }

        describe("deserialization") {
            it("gets the correct result from stellar java xdr data type binary output") {
                val tinyXdr = Int64().apply { int64 = tiny }
                val tinyOutputStream = ByteArrayOutputStream()
                tinyXdr.encode(XdrDataOutputStream(tinyOutputStream))
                val tinyResult = Xdr.load(Long.serializer(), tinyOutputStream.toByteArray())
                assertEquals(expected = tinyXdr.int64, actual = tinyResult)

                val hugeXdr = Int64().apply { int64 = huge }
                val hugeOutputStream = ByteArrayOutputStream()
                hugeXdr.encode(XdrDataOutputStream(hugeOutputStream))
                val hugeResult = Xdr.load(Long.serializer(), hugeOutputStream.toByteArray())
                assertEquals(expected = hugeXdr.int64, actual = hugeResult)

                val zeroXdr = Int64().apply { int64 = zero }
                val zeroOutputStream = ByteArrayOutputStream()
                zeroXdr.encode(XdrDataOutputStream(zeroOutputStream))
                val zeroResult = Xdr.load(Long.serializer(), zeroOutputStream.toByteArray())
                assertEquals(expected = zeroXdr.int64, actual = zeroResult)

                val minusOneXdr = Int64().apply { int64 = minusOne }
                val minusOneOutputStream = ByteArrayOutputStream()
                minusOneXdr.encode(XdrDataOutputStream(minusOneOutputStream))
                val minusOneResult = Xdr.load(Long.serializer(), minusOneOutputStream.toByteArray())
                assertEquals(expected = minusOneXdr.int64, actual = minusOneResult)

                val plusOneXdr = Int64().apply { int64 = plusOne }
                val plusOneOutputStream = ByteArrayOutputStream()
                plusOneXdr.encode(XdrDataOutputStream(plusOneOutputStream))
                val plusOneResult = Xdr.load(Long.serializer(), plusOneOutputStream.toByteArray())
                assertEquals(expected = plusOneXdr.int64, actual = plusOneResult)

            }
        }
    }
})

//TODO: Test Floats

//TODO: Test Booleans

object StringSerializationSpec : Spek({
    describe("Some Strings") {
        val byteString32 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdef"
        val byteString64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz.,/;'[]-!#*&"


        describe("32 byte serialization") {
            it("can be reformed using stellar java xdr data types") {
                val byteString32Bytes = Xdr.dump(String.serializer(), byteString32)
                val byteString32LoadResult = String32.decode(XdrInputStream(byteString32Bytes))
                assertEquals(expected = byteString32, actual = byteString32LoadResult.string32)
            }

            it("gets the correct result from stellar java xdr data type binary output") {
                val byteString32Xdr = String32().apply { string32 = byteString64 }
                val byteString32OutputStream = ByteArrayOutputStream()
                byteString32Xdr.encode(XdrDataOutputStream(byteString32OutputStream))
                val byteString32Result = Xdr.load(String.serializer(), byteString32OutputStream.toByteArray())
                assertEquals(expected = byteString32Xdr.string32, actual = byteString32Result)
            }
        }

        describe("64 byte serialization") {
            it("can be reformed using stellar java xdr data types") {
                val byteString64Bytes = Xdr.dump(String.serializer(), byteString64)
                val byteString64LoadResult = String64.decode(XdrInputStream(byteString64Bytes))
                assertEquals(expected = byteString64, actual = byteString64LoadResult.string64)
            }

            it("gets the correct result from stellar java xdr data type binary output") {
                val byteString64Xdr = String64().apply { string64 = byteString64 }
                val byteString64OutputStream = ByteArrayOutputStream()
                byteString64Xdr.encode(XdrDataOutputStream(byteString64OutputStream))
                val byteString64Result = Xdr.load(String.serializer(), byteString64OutputStream.toByteArray())
                assertEquals(expected = byteString64Xdr.string64, actual = byteString64Result)
            }
        }
    }
})

//TODO: Test Enums