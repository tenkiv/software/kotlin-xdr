package tenkiv.xdr

import kotlinx.io.*
import kotlinx.serialization.*
import kotlinx.serialization.modules.*

//TODO: numElements should be changed to unsignedInt
@SerialInfo
@Target(AnnotationTarget.PROPERTY)
public annotation class FixedLengthXdrArray(val numElements: Int)

/**
 * Indicates that the enum class uses custom identifiers instead of the ordinal for each enum when serialized to xdr.
 * This annotation indicates that all enum values must be marked with [XdrEnumIdentifier]. You cannot use a custom
 * identifier for some values of an enum class and the ordinal for others. All values of a particular enum class
 * must either use their ordinal value as their xdr identifier (this is the default implicit behavior if no annotations
 * are used) or use custom [XdrEnumIdentifier] for all enum values.
 */
@SerialInfo
@Target(AnnotationTarget.CLASS)
public annotation class XdrCustomEnumIdentifiers

@SerialInfo
@Target(AnnotationTarget.PROPERTY)
public annotation class XdrEnumIdentifier(val identifier: Int)

/**
 * Used to mark the base type for a Polymorphic serializer (normally a sealed class) to use an [Int] instead of a
 * [String] to identify the concrete type being serialized / deserialized to / from XDR. All serializable subtypes
 * must have a serial name that is a valid representation of a number (e.g. 0, 1, 2).
 */
@SerialInfo
@Target(AnnotationTarget.CLASS)
public annotation class XdrUnion

public class Xdr(context: SerialModule = EmptyModule) : AbstractSerialFormat(context), BinaryFormat {

    override fun <T> dump(serializer: SerializationStrategy<T>, obj: T): ByteArray {
        val output = ByteArrayOutputStream()
        val dumper = Writer(XdrEncoder(output))
        dumper.encode(serializer, obj)
        return output.toByteArray()
    }

    override fun <T> load(deserializer: DeserializationStrategy<T>, bytes: ByteArray): T {
        val stream = ByteArrayInputStream(bytes)
        val reader = Reader(XdrDecoder(stream))
        return reader.decode(deserializer)
    }

//▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬//
    //   ⎍⎍⎍⎍⎍⎍⎍⎍   ஃ Encoding ஃ   ⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍    //
    //▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬//

    private open inner class Writer(val encoder: XdrEncoder) : ElementValueEncoder() {

        private var nextCollectionSize: Int? = null

        override val context: SerialModule
            get() = this@Xdr.context


        override fun encodeBoolean(value: Boolean) = encoder.encodeBoolean(value)

        override fun encodeByte(value: Byte) = encoder.encodeByte(value)

        override fun encodeChar(value: Char) = encoder.encodeInt(value.toInt())

        override fun encodeDouble(value: Double) = encoder.encodeDouble(value)

        override fun encodeFloat(value: Float) = encoder.encodeFloat(value)

        override fun encodeInt(value: Int) = encoder.encodeInt(value)

        override fun encodeLong(value: Long) = encoder.encodeLong(value)

        override fun encodeNotNullMark() {
            // insert true before value
            encoder.encodeBoolean(true)
        }

        override fun encodeNull() {
            // insert false followed by the next item as null / void is not encoded outside optional types.
            encoder.encodeBoolean(false)
        }

        override fun encodeShort(value: Short) = encoder.encodeInt(value.toInt())

        override fun encodeString(value: String) = encoder.encodeString(value)

        override fun encodeElement(desc: SerialDescriptor, index: Int): Boolean {
            val elementDesc = desc.getElementDescriptor(index)

            if (elementDesc.kind == StructureKind.LIST || elementDesc.kind == StructureKind.MAP) {
                nextCollectionSize = desc
                    .getElementAnnotations(index)
                    .filterIsInstance<FixedLengthXdrArray>()
                    .singleOrNull()
                    ?.numElements
            }

            return true
        }

        override fun shouldEncodeElementDefault(desc: SerialDescriptor, index: Int): Boolean = true

        override fun beginCollection(
            desc: SerialDescriptor,
            collectionSize: Int,
            vararg typeParams: KSerializer<*>
        ): CompositeEncoder {

            val fixedCollectionSize = nextCollectionSize
            // If the collection length is variable
            if (fixedCollectionSize == null) {
                if (desc.kind == StructureKind.MAP) {
                    encodeInt(collectionSize * 2)
                } else {
                    encodeInt(collectionSize)
                }
            }

            return this
        }

        override fun beginStructure(desc: SerialDescriptor, vararg typeParams: KSerializer<*>): CompositeEncoder {
            return if (desc.kind is PolymorphicKind) {
                if (desc.getEntityAnnotations().any { it is XdrUnion }) {
                    XdrUnionWriter(PolymorphicWriter(encoder))
                } else {
                    PolymorphicWriter(encoder)
                }
            } else {
                this
            }
        }

        // Unit maps to Xdr void
        override fun encodeUnit() {}

        override fun encodeEnum(enumDescription: SerialDescriptor, ordinal: Int) {
            encoder.encodeInt(
                enumDescription
                    .getElementAnnotations(ordinal)
                    .filterIsInstance<XdrEnumIdentifier>()
                    .singleOrNull()
                    ?.identifier ?: ordinal
            )
        }

    }

    private inner class PolymorphicWriter(encoder: XdrEncoder) : Writer(encoder) {

        override fun encodeElement(desc: SerialDescriptor, index: Int): Boolean = true

    }

    private inner class XdrUnionWriter(private val baseWriter: PolymorphicWriter) : Encoder by baseWriter,
        CompositeEncoder by baseWriter {

        override val context: SerialModule
            get() = this@Xdr.context

        override fun encodeStringElement(desc: SerialDescriptor, index: Int, value: String) =
            if (index == 0) {
                encodeInt(
                    value.toIntOrNull() ?: throw SerializationException(
                        "SerialName of concrete types of XdrUnion must be a valid " +
                                "representation of a number"
                    )
                )
            } else {
                baseWriter.encodeStringElement(desc, index, value)
            }
    }

    class XdrEncoder(val output: OutputStream) {

        // Null / void is empty, there is no null / void byte

        fun encodeByte(value: Byte) {
            output.write(value.toInt())
        }

        fun encodeBoolean(value: Boolean) = when (value) {
            false -> encodeInt(0)
            true -> encodeInt(1)
        }

        fun encodeInt(value: Int) {
            output.write(ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(value).array())
        }

        fun encodeLong(value: Long) {
            output.write(ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN).putLong(value).array())
        }

        fun encodeFloat(value: Float) = encodeInt(value.toBits())

        fun encodeDouble(value: Double) = encodeLong(value.toBits())

        fun encodeString(value: String) {
            //TODO: This should to be an unsigned integer.
            val bytes = value.toUtf8Bytes()
            encodeInt(bytes.size)
            output.write(bytes)
        }
    }

    //▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬//
    //   ⎍⎍⎍⎍⎍⎍⎍⎍   ஃ Decoding ஃ   ⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍⎍    //
    //▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬▬//

    private open inner class Reader(val decoder: XdrDecoder) : ElementValueDecoder() {

        override val context: SerialModule
            get() = this@Xdr.context

        override val updateMode: UpdateMode
            get() = UpdateMode.BANNED

        protected var readProperties: Int = 0

        private var _numElements: Int? = null

        protected open val numElements: Int
            get() = _numElements ?: throw XdrDecodingException(NUM_ELEMENTS_ACCESS_MSG)

        private var nextCollectionSize: Int? = null

        override fun decodeBoolean(): Boolean = decoder.nextBoolean()

        override fun decodeByte(): Byte = decoder.nextByte()

        override fun decodeChar(): Char = decoder.nextInt().toChar()

        override fun decodeDouble(): Double = decoder.nextDouble()

        override fun decodeFloat(): Float = decoder.nextFloat()

        override fun decodeInt(): Int = decoder.nextInt()

        override fun decodeLong(): Long = decoder.nextLong()

        override fun decodeNotNullMark(): Boolean = decodeBoolean()

        override fun decodeShort(): Short = decodeInt().toShort()

        // Unit maps to xdr void
        override fun decodeUnit() {}

        override fun decodeString(): String = decoder.nextString()

        override fun decodeEnum(enumDescription: SerialDescriptor): Int {

            val usesCustomIdentifiers = enumDescription
                .getEntityAnnotations()
                .any { it is XdrCustomEnumIdentifiers }

            val numValues = enumDescription.elementsCount

            val enumIdentifier = decodeInt()

            if (usesCustomIdentifiers) {
                for (i in 0 until numValues) {
                    val specialIdentifier = enumDescription
                        .getElementAnnotations(i)
                        .filterIsInstance<XdrEnumIdentifier>()
                        .firstOrNull() ?: throw XdrDecodingException(
                        "Enum marked as using custom xdr identifiers cannot have an element without an identifier"
                    )

                    if (specialIdentifier.identifier == enumIdentifier) {
                        return i
                    }
                }
                throw XdrDecodingException(
                    "Enum marked as using custom xdr identifiers has loaded an identifier for " +
                            "which there is no matching annotation"
                )
            } else {
                return enumIdentifier
            }
        }

        protected fun setNextCollectionSize(desc: SerialDescriptor) {
            if (desc.kind !is PolymorphicKind && readProperties < numElements) {
                nextCollectionSize = desc.getElementAnnotations(readProperties)
                    .filterIsInstance<FixedLengthXdrArray>()
                    .singleOrNull()?.numElements
            }
        }

        private fun setNumElements(desc: SerialDescriptor) {
            if (_numElements == null) {
                if (desc.kind is PolymorphicKind) {
                    _numElements = 2
                } else if (!(desc.kind == StructureKind.MAP || desc.kind == StructureKind.LIST)) {
                    _numElements = desc.elementsCount
                }
            }
        }

        override fun decodeElementIndex(desc: SerialDescriptor): Int {
            setNumElements(desc)
            setNextCollectionSize(desc)
            val currentReadProperties = readProperties

            readProperties++

            return if (currentReadProperties == numElements) CompositeDecoder.READ_DONE else currentReadProperties
        }

        override fun beginStructure(desc: SerialDescriptor, vararg typeParams: KSerializer<*>): CompositeDecoder {
            setNumElements(desc)
            return when {
                desc.kind == StructureKind.LIST || desc.kind == StructureKind.MAP -> {
                    CollectionReader(decoder, nextCollectionSize)
                }
                desc.getEntityAnnotations().any { it is XdrUnion } -> XdrUnionReader(Reader(decoder))
                else -> Reader(decoder)
            }
        }
    }

    private inner class CollectionReader(decoder: XdrDecoder, private var collectionSize: Int?) : Reader(decoder) {

        protected override val numElements: Int
            get() = collectionSize ?: throw XdrDecodingException(NUM_ELEMENTS_ACCESS_MSG)


        override fun decodeCollectionSize(desc: SerialDescriptor): Int = collectionSize ?: run {
            val variableCollectionSize = decodeInt()
            collectionSize = variableCollectionSize
            variableCollectionSize
        }

    }

    private inner class XdrUnionReader(private val baseReader: Reader) : Decoder by baseReader,
        CompositeDecoder by baseReader {

        override val context: SerialModule
            get() = this@Xdr.context

        override val updateMode: UpdateMode
            get() = baseReader.updateMode

        override fun decodeStringElement(desc: SerialDescriptor, index: Int): String =
            if (index == 0) {
                baseReader.decodeInt().toString()
            } else {
                baseReader.decodeStringElement(desc, index)
            }
    }

    class XdrDecoder(val input: InputStream) {

        fun nextByte(): Byte = input.read().toByte()

        fun nextBoolean(): Boolean = when (nextInt()) {
            0 -> false
            1 -> true
            else -> throw XdrDecodingException("Int representation does not correspond to Boolean.")
        }

        fun nextInt(): Int = input.readToByteBuffer(4).order(ByteOrder.BIG_ENDIAN).getInt()

        fun nextLong(): Long = input.readToByteBuffer(8).order(ByteOrder.BIG_ENDIAN).getLong()

        fun nextFloat(): Float = Float.fromBits(nextInt())

        fun nextDouble(): Double = Double.fromBits(nextLong())

        fun nextString(): String {
            val strNumBytes = nextInt()
            val stringBytes = input.readExactNBytes(strNumBytes)

            return stringFromUtf8Bytes(stringBytes)
        }

    }

    companion object : BinaryFormat {

        internal const val NUM_ELEMENTS_ACCESS_MSG =
            "The total number of elements for this structure must be set before being accessed."

        val plain = Xdr()

        override fun <T> dump(serializer: SerializationStrategy<T>, obj: T): ByteArray = plain.dump(serializer, obj)

        override fun <T> load(deserializer: DeserializationStrategy<T>, bytes: ByteArray): T =
            plain.load(deserializer, bytes)

        override fun install(module: SerialModule) =
            throw IllegalStateException("You should not install anything to global instance")

        override val context: SerialModule get() = plain.context
    }

}

class XdrDecodingException(message: String) : SerializationException(message)
