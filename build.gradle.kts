buildscript {
    repositories {
        mavenCentral()
        jcenter()
    }

    dependencies {
        classpath(group = "org.jetbrains.kotlin", name = "kotlin-serialization", version = Vof.kotlin)
    }
}

plugins {
    kotlin("multiplatform") version Vof.kotlin
    id("org.jetbrains.kotlin.plugin.serialization") version Vof.kotlin
    id("maven-publish")
    signing
}

val isRelease = isRelease()
val properties = createPropertiesFromLocal()
setSigningExtrasFromProperties(properties)

repositories {
    mavenCentral()
    jcenter()
    maven(url = "https://jitpack.io")
}

kotlin {
    jvm()
    js()

    sourceSets {
        all {
            languageSettings.useExperimentalAnnotation("kotlin.ExperimentalStdlibApi")
        }

        val commonMain by getting {
            dependencies {
                implementation(kotlin("stdlib-common"))
                api("org.jetbrains.kotlinx:kotlinx-serialization-runtime-common:${Vof.serialization}")
            }
        }
        val commonTest by getting {
            dependencies {
                implementation(kotlin("test-common"))
                implementation(kotlin("test-annotations-common"))
                implementation("org.spekframework.spek2:spek-dsl-metadata:${Vof.spek}")
            }
        }

        val jvmMain by getting {
            dependencies {
                implementation(kotlin("stdlib-jdk8"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime:${Vof.serialization}")
            }
        }

        val jvmTest by getting {
            dependencies {
                implementation("org.jetbrains.kotlin:kotlin-test:${Vof.kotlin}")
                implementation("org.jetbrains.kotlin:kotlin-test-junit:${Vof.kotlin}")
                implementation("org.spekframework.spek2:spek-dsl-jvm:${Vof.spek}")

                implementation("com.github.stellar:java-stellar-sdk:${Vof.javaStellarSdk}")

                runtimeOnly("org.spekframework.spek2:spek-runner-junit5:${Vof.spek}")
                runtimeOnly(kotlin("reflect"))
            }
        }

        val jsMain by getting {
            dependencies {
                implementation(kotlin("stdlib-js"))
                implementation("org.jetbrains.kotlinx:kotlinx-serialization-runtime-js:${Vof.serialization}")
            }
        }

        val jsTest by getting {
            dependencies {
                implementation(kotlin("test-js"))
                implementation("org.spekframework.spek2:spek-dsl-js:${Vof.spek}")
            }
        }
    }

    tasks {
        registerCommonTasks()
    }

    publishing {
        publications.withType<MavenPublication>().forEach {
            it.configureMavenPom(isRelease, project)
            signing { if (isRelease) sign(it) }
        }

        setMavenRepositories(isRelease, properties)
    }
}